<?php

namespace Wabeo\Sirsag;

/**
 * Plugin name: Simple RSS Agregator
 * Description: Merge multiple RSS feed into a new one
 * Author: willybahuaud
 * Text Domain: simple-rss-agregator
 * Domain Path: /languages
 * Author URI: https://wabeo.fr
 * Version: 1.0
 * Stable tag: 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( esc_html( __( 'Cheatin&#8217; uh?' ) ) );
}

/**
 * Load language pack
 */
add_action( 'plugins_loaded', __NAMESPACE__ . '\load_languages' );
function load_languages() {
	load_plugin_textdomain( 'simple-rss-agregator', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
}

/**
 * Register post type
 */
add_action( 'init', __NAMESPACE__ . '\register_feed_post_type', 0 );
function register_feed_post_type() {

	$labels = array(
		'name'                  => _x( 'Feeds', 'post type general name', 'simple-rss-agregator' ),
		'singular_name'         => _x( 'Feed', 'post type singular name', 'simple-rss-agregator' ),
		'menu_name'             => _x( 'Simple RSS Agregator', 'post type general name', 'simple-rss-agregator' ),
		'all_items'             => __( 'All Feeds', 'simple-rss-agregator' ),
		'add_new'               => _x( 'Add New', 'feed', 'simple-rss-agregator' ),
		'add_new_item'          => __( 'Add New Feed', 'simple-rss-agregator' ),
		'edit_item'             => __( 'Edit Feed', 'simple-rss-agregator' ),
		'new_item'              => __( 'New Feed', 'simple-rss-agregator' ),
		'view_item'             => __( 'View Feed', 'simple-rss-agregator' ),
		'items_archive'         => _x( 'Feeds', 'post type general name', 'simple-rss-agregator' ),
		'search_items'          => __( 'Search Feeds', 'simple-rss-agregator' ),
		'not_found'             => __( 'No feeds found.', 'simple-rss-agregator' ),
		'not_found_in_trash'    => __( 'No feeds found in Trash.', 'simple-rss-agregator' ),
		'parent_item_colon'     => __( 'Parent Feed:', 'simple-rss-agregator' ),
		'archives'              => __( 'Feed Archives', 'simple-rss-agregator' ),
		'insert_into_item'      => __( 'Insert into feed', 'simple-rss-agregator' ),
		'uploaded_to_this_item' => __( 'Uploaded to this feed', 'simple-rss-agregator' ),
		'filter_items_list'     => __( 'Filter feeds list', 'simple-rss-agregator' ),
		'items_list_navigation' => __( 'Feeds list navigation', 'simple-rss-agregator' ),
		'items_list'            => __( 'Feeds list', 'simple-rss-agregator' ),
		'featured_image'        => __( 'Feed Picture', 'simple-rss-agregator' ),
		'set_featured_image'    => __( 'Set feed picture', 'simple-rss-agregator' ),
		'remove_featured_image' => __( 'Remove feed picture', 'simple-rss-agregator' ),
		'use_featured_image'    => __( 'Use as feed picture', 'simple-rss-agregator' ),
	);

	register_post_type( 'feed', array(
		'label'           => _x( 'Feeds', 'post type general name', 'simple-rss-agregator' ),
		'labels'          => $labels,
		'public' => false,
		'show_ui' => true,
		'has_archive' => false,
		'menu_icon' => 'dashicons-rss',
		'exclude_from_search' => true,
		'supports' => array( 'title', 'thumbnail', 'excerpt' ),
	) );
}


/**
 * Translate post type message
 */
add_filter( 'post_updated_messages', __NAMESPACE__ . '\post_feed_type_messages' );
function post_feed_type_messages( $messages ) {
	global $post;

	if ( 'feed' === $post->post_type ) {

		$post_url    = get_permalink( $post->ID );
		$preview_url = esc_url( add_query_arg( 'preview', 'true', $post_url ) );
		$post_url    = esc_url( $post_url );

		$messages[ $post->post_type ] = array(
			 0 => '', // Unused. Messages start at index 1.
			 1 => sprintf( __( 'Feed updated. <a href=\'%s\'>View feed</a>', 'simple-rss-agregator' ), $post_url ),
			 2 => __( 'Custom field updated.' ),
			 3 => __( 'Custom field deleted.' ),
			 4 => __( 'Feed updated.', 'simple-rss-agregator' ),
			/* translators: %s: date and time of the revision */
			 5 => isset( $_GET['revision'] ) ? sprintf( __( 'Feed restored to revision from %s', 'simple-rss-agregator' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
			 6 => sprintf( __( 'Feed published. <a href=\'%s\'>View feed</a>', 'simple-rss-agregator' ), $post_url ),
			 7 => __( 'Feed saved.', 'simple-rss-agregator' ),
			 8 => sprintf( __( 'Feed submitted. <a target=\'_blank\' href=\'%s\'>Preview feed</a>', 'simple-rss-agregator' ), $preview_url ),
			 9 => sprintf( __( 'Feed scheduled for: <strong>%1$s</strong>. <a target=\'_blank\' href=\'%2$s\'>Preview feed</a>', 'simple-rss-agregator' ), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), $post_url ),
			10 => sprintf( __( 'Feed draft updated. <a target=\'_blank\' href=\'%s\'>Preview feed</a>', 'simple-rss-agregator' ), $preview_url ),
		);

	}

	return $messages;
}

/**
 * Translate update post type message
 */
add_filter( 'bulk_post_updated_messages', __NAMESPACE__ . '\bulk_post_feed_type_messages', 10, 2 );
function bulk_post_feed_type_messages( $bulk_messages, $bulk_counts ) {
	$bulk_messages['feed'] = array(
		'updated'   => _n( '%s feed updated.', '%s feeds updated.', $bulk_counts['updated'], 'simple-rss-agregator' ),
		'locked'    => _n( '%s feed not updated, somebody is editing it.', '%s feeds not updated, somebody is editing them.', $bulk_counts['locked'], 'simple-rss-agregator' ),
		'deleted'   => _n( '%s feed permanently deleted.', '%s feeds permanently deleted.', $bulk_counts['deleted'], 'simple-rss-agregator' ),
		'trashed'   => _n( '%s feed moved to the Trash.', '%s feeds moved to the Trash.', $bulk_counts['trashed'], 'simple-rss-agregator' ),
		'untrashed' => _n( '%s feed restored from the Trash.', '%s feeds restored from the Trash.', $bulk_counts['untrashed'], 'simple-rss-agregator' ),
	);

	return $bulk_messages;
}


/**
 * Register metabox
 */
add_action( 'add_meta_boxes_feed', __NAMESPACE__ . '\feed_metabox', 10, 2 );
function feed_metabox( $post ) {
	add_meta_box( 'website_uri', __( 'Website URI', 'simple-rss-agregator' ), __NAMESPACE__ . '\metabox', 'feed', 'side', 'default', array(
		'name'  => '_website_uri',
		'id'    => $post->ID,
		'label' => __( 'Add website URI here', 'simple-rss-agregator' ),
	) );

	add_meta_box( 'feed_uri', __( 'Feed URI', 'simple-rss-agregator' ), __NAMESPACE__ . '\metabox', 'feed', 'side', 'default', array(
		'name'  => '_feed_uri',
		'id'    => $post->ID,
		'label' => __( 'Add website feed URI here', 'simple-rss-agregator' ),
	) );
}


/**
 * Metabox callback
 */
function metabox( $post, $opt ) {
	$old = get_post_meta( $post->ID, $opt['args']['name'], true );
	echo '<p><label for="' . esc_attr( $opt['args']['name'] ) . '">' . esc_html( $opt['args']['label'] ) . '</label>';
	echo '<input type="url" value="' . esc_attr( $old ) . '" 
		id="' . esc_attr( $opt['args']['name'] ) . '" 
		name="' . esc_attr( $opt['args']['name'] ) . '" 
		class="widefat"/></p>';
	wp_nonce_field( "update-{$post->ID}-{$opt['args']['name']}-" . get_current_user_id(), "update-{$opt['args']['name']}" );
}


/**
 * Save metabox
 */
add_action( 'save_post_feed', __NAMESPACE__ . '\save_metabox', 10, 2 );
function save_metabox( $post_id, $post ) {
	$meta = array( '_website_uri', '_feed_uri' );
	foreach ( $meta as $m ) {
		if ( isset( $_POST[ $m ], $_POST['post_ID'], $_POST[ "update-{$m}" ] ) ) {
			$id = intval( $_POST['post_ID'] );
			if ( wp_verify_nonce( $_POST[ "update-{$m}" ], "update-{$id}-{$m}-" . get_current_user_id() ) ) {
				update_post_meta( $post_id, $m, esc_url( $_POST[ $m ] ) );
			}
		}
	}
}

/**
 * Schedule an Cron for update feed
 */

if ( ! wp_next_scheduled( 'sirsag_update' ) ) {
	wp_schedule_event( time(), 'twicedaily', 'sirsag_update' );
}


/**
 * Update feed
 */
add_action( 'sirsag_update', __NAMESPACE__ . '\merge_feeds' );
function merge_feeds() {
	$args = array(
		'post_type'        => 'feed',
		'posts_per_page'   => 999,
		'post_status'      => 'publish',
		'suppress_filters' => true,
		'fields'           => 'ids',
	);
	$sites = get_posts( $args );
	$feeds = array_filter( array_map( __NAMESPACE__ . '\retrieve_feed', $sites ) );

	include_once( ABSPATH . WPINC . '/feed.php' );
	$feeds = fetch_feed( $feeds );

	if ( ! is_wp_error( $feeds ) ) {
		$maxitems  = $feeds->get_item_quantity( get_option( 'sirsag_items_count', 25 ) );
		$rss_items = $feeds->get_items( 0, $maxitems );

		delete_feed_thumbnails();
		$opt = array_map( __NAMESPACE__ . '\parse_feed', $rss_items );
		update_option( 'sirsag_feeds', $opt );
	}
}


/**
 * Manual feed update
 */
add_action( 'admin_post_sirsag.update_cache', __NAMESPACE__ . '\update_cache' );
function update_cache() {
	merge_feeds();
	update_option( 'sirsag_cache_updated', 'yes' );
	wp_redirect( admin_url( 'admin.php?page=sirsag_feed' ) );
	exit();
}


/**
 * Get feed URI for a feed post
 */
function retrieve_feed( $site ) {
	$feed_uri = get_post_meta( $site, '_feed_uri', true );
	return $feed_uri;
}


/**
 * Parse simple pie item
 */
function parse_feed( $feed ) {
	$feed_formatted = array(
		'title'       => $feed->get_title(),
		'link'        => $feed->get_permalink(),
		'date'        => $feed->get_date( 'r' ),
		'creator'     => '',
		'description' => htmlspecialchars( strip_tags( $feed->get_description() ) ),
		'content'     => $feed->get_content(),
	);

	if ( $feed_link = $feed->get_feed()->get_link() ) {
		$link = get_sitename_by_url( $feed_link );
		$feed_formatted['creator'] = $link;
	} elseif ( $author = $feed->get_author() ) {
		$feed_formatted['creator'] = $author->get_name();
	}

	if ( $img = get_image( $feed_formatted['link'] ) ) {
		$feed_formatted['thumbnail'] = $img;
	}

	return $feed_formatted;
}


/**
 * Print merged feed
 */
function wrap_feed() {
	$feeds = get_option( 'sirsag_feeds', array() );

	$feedlink  = home_url( get_option( 'sirsag_url', 'main-feed' ) );
	$feedhome  = home_url( '/' );
	$feedtitle = get_option( 'sirsag_title', '' );
	$feeddesc  = get_option( 'sirsag_desc', '' );
	$lang      = get_option( 'sirsag_lang', 'en-US' );

	echo '<?xml version="1.0" encoding="UTF-8"?>';
	?><rss version="2.0" 
    xmlns:atom="http://www.w3.org/2005/Atom"
    xmlns:content="http://purl.org/rss/1.0/modules/content/" 
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:creativeCommons="http://backend.userland.com/creativeCommonsRssModule"
	>
		<channel>
		<title><?php echo esc_html( $feedtitle ); ?></title>
		<atom:link href="<?php echo esc_url( $feedlink ); ?>" rel="self" type="application/rss+xml" />
		<link><?php echo esc_url( $feedhome ); ?></link>
		<description><?php echo esc_html( $feeddesc ); ?></description>
		<language><?php echo esc_html( $lang ); ?></language>
		<creativeCommons:license>http://creativecommons.org/licenses/by-nc-sa/3.0/</creativeCommons:license>
		<?php

		foreach ( $feeds as $feed ) {
			?>
			<item>
		        <title><?php echo esc_html( $feed['title'] ); ?></title>
		        <link><?php echo esc_html( $feed['link'] ); ?></link>
		        <guid><?php echo esc_html( $feed['link'] ); ?></guid>
		        <pubDate><?php echo esc_html( $feed['date'] ); ?></pubDate>
		        <dc:creator><?php echo esc_html( $feed['creator'] ); ?></dc:creator>
		        <description>
		        <?php echo wp_kses_post( $feed['description'] ); ?>
		        </description>
		        <content:encoded><![CDATA[<?php echo wp_kses_post( $feed['content'] ); ?>]]></content:encoded>
		    </item>
			<?php
		}

		?>
		</channel>
	</rss>
	<?php
}


/**
 * Find 1 image into feed content
 */
function get_image( $link ) {

	$src = wp_remote_get( $link );
	if ( is_wp_error( $src ) || 200 != wp_remote_retrieve_response_code( $src ) ) {
		return false;
	}

	$content = wp_remote_retrieve_body( $src );

	$re = "/<meta.*?property=[\\'\"]og:image[\\'\"].*?content=[\\'\"]([^\"']*)[\\'\"].*?\\/?>/";
	if ( preg_match( $re, $content, $og ) ) {
		$imgprob = $og[1];
		if ( $imgprob = import_image( $imgprob ) ) {
			return $imgprob;
		}
	}

	preg_match_all( '/<img.*?src=[\'"](.*?)[\'"].*?>/xis', $content, $matches );

	$i = 0;
	while ( ! empty( $matches[1][ $i ] ) ) {
		$image_found = urldecode( trim( $matches[1][ $i ] ) );
		if ( stripos( $image_found, '//' ) === 0 ) {
		    $image_found = 'http:' . $image_found;
		}

		if ( stripos( $image_found, 'fbcdn' ) > 0 ) {
		    $ext = strrchr( $image_found, '.' );
		    $fb_larger_img = str_replace( '_s' . $ext, '_n' . $ext , $image_found );
		    $fb_img = wp_remote_get( $fb_larger_img );
			if ( 200 == wp_remote_retrieve_response_code( $fb_img ) ) {
		        $image_found = $fb_larger_img;
		    }
		}
		$image_url_host = parse_url( $image_found, PHP_URL_HOST );
		if ( 'fbexternal-a.akamaihd.net' === $image_url_host ) {
		    $query_str = parse_url( $image_found, PHP_URL_QUERY );
		    if ( '' !== $query_str ) {
		        parse_str( urldecode( $query_str ), $output );

		        if ( isset( $output['amp;url'] ) ) {
		        	$output['url'] = $output['amp;url'];
		        }
		        if ( isset( $output['url'] ) ) {
		            $image_found = urldecode( $output['url'] );
		        }
		    }
		}

		if ( $image_found = import_image( $image_found ) ) {
			return $image_found;
			break;
		}

		$i++;
	}
	return false;
}


/**
 * Import feed image into WordPress
 */
function import_image( $url, $desc = '', $post_id ) {
	if ( ! function_exists( 'media_handle_sideload' ) ) {
		require_once( ABSPATH . 'wp-admin/includes/image.php' );
		require_once( ABSPATH . 'wp-admin/includes/file.php' );
		require_once( ABSPATH . 'wp-admin/includes/media.php' );
	}

	$tmp = download_url( $url );
	if ( is_wp_error( $tmp ) ) {
		return false;
	}
	$file_array = array();

	preg_match( '/[^\?]+\.(jpg|jpe|jpeg|gif|png|pdf)/i', $url, $matches );
	$file_array['name'] = basename( $matches[0] );
	$file_array['tmp_name'] = $tmp;

	if ( is_wp_error( $tmp ) ) {
		@unlink( $file_array['tmp_name'] );
		$file_array['tmp_name'] = '';
	}

	$min_width = 150;
	$min_height = 150;
	$size = getimagesize( $file_array['tmp_name'] );
	if ( ! $size || $size[0] < $min_width || $size[1] < $min_height ) {
	    @unlink( $file_array['tmp_name'] );
	    return false;
	}

	$id = media_handle_sideload( $file_array, $post_id, $desc );
	if ( is_wp_error( $id ) ) {
		@unlink( $file_array['tmp_name'] );
		return $id;
	}

	add_post_meta( $id, 'sirsag_uploaded', true );
	return $id;
}


/**
 * Delete old feed image
 */
function delete_feed_thumbnails() {
	$thumbs = get_posts( array(
		'suppress_filters' => false,
		'posts_per_page'   => 99,
		'post_type'        => 'attachment',
		'fields'           => 'ids',
		'no_found_rows'    => 1,
		'meta_query'       => array(
			array(
				'key'   => 'sirsag_uploaded',
				'value' => true,
			),
		),
	) );
	foreach ( $thumbs as $thumb ) {
		wp_delete_attachment( $thumb, true );
	}
}


/**
 * Add rewrite rule
 */
add_action( 'init', __NAMESPACE__ . '\rewrite_rule' );
function rewrite_rule() {
	add_rewrite_rule( '^' . get_option( 'sirsag_url', 'main-feed' ) . '/?$', 'index.php?displaysirsagfeed=1', 'top' );
}

/**
 * Flush rewrite rules on activation and desactivation
 */
register_deactivation_hook( __FILE__, 'flush_rewrite_rules' );
register_activation_hook( __FILE__, __NAMESPACE__ . '\do_flush_rewrites' );
function do_flush_rewrites() {
	rewrite_rule();
	flush_rewrite_rules();
}

/**
 * Register a `displaysirsagfeed` query var
 */
add_filter( 'query_vars', __NAMESPACE__ . '\query_vars' );
function query_vars( $vars ) {
	$vars[] = 'displaysirsagfeed';
	return $vars;
}

/**
 * Display RSS Feed
 */
add_action( 'template_redirect', __NAMESPACE__ . '\display_feed' );
function display_feed( $template ) {
	if ( 1 == get_query_var( 'displaysirsagfeed' ) ) {
		wrap_feed();
		exit();
	}
	return $template;
}

/**
 * Set RSS feed Headers
 */
add_filter( 'wp_headers', __NAMESPACE__ . '\headers' );
function headers( $headers ) {
	if ( 1 == get_query_var( 'displaysirsagfeed' ) ) {
		$headers['Content-Type'] = 'application/rss+xml';
	}
	return $headers;
}

/**
 * Add setting page
 */
add_action( 'admin_menu', __NAMESPACE__ . '\add_setting_page' );
function add_setting_page() {
	add_submenu_page( 'edit.php?post_type=feed', __( 'Settings', 'simple-rss-agregator' ), __( 'Settings', 'simple-rss-agregator' ), 'manage_options', 'sirsag_feed', __NAMESPACE__ . '\setting_page' );
}


/**
 * Register settings
 */
add_action( 'admin_init', __NAMESPACE__ . '\register_settings' );
function register_settings() {
	register_setting( 'sirsag_feed', 'sirsag_lang' );
	register_setting( 'sirsag_feed', 'sirsag_items_count' );
	register_setting( 'sirsag_feed', 'sirsag_url' );
	register_setting( 'sirsag_feed', 'sirsag_title' );
	register_setting( 'sirsag_feed', 'sirsag_desc' );
}


/**
 * Do setting page
 */
function setting_page() {
	echo '<div class="wrap">';
		echo '<h2>' . esc_html( __( 'Simple RSS agregator settings', 'simple-rss-agregator' ) ) . '</h2>';

		add_settings_section( 'sirsag_setting_section',
			__( 'Feed information', 'simple-rss-agregator' ),
			'__return_false',
		'sirsag_feed' );

		add_settings_field( 'sirsag_url',
			__( 'Relative URL of the merged feed', 'simple-rss-agregator' ),
			__NAMESPACE__ . '\setting_callback_function',
			'sirsag_feed',
			'sirsag_setting_section',
			array(
				'type'        => 'text',
				'placeholder' => 'main-feed',
				'name'        => 'sirsag_url',
				'default'     => 'main-feed',
			)
		);

		add_settings_field( 'sirsag_title',
			__( 'Title of the merged feed', 'simple-rss-agregator' ),
			__NAMESPACE__ . '\setting_callback_function',
			'sirsag_feed',
			'sirsag_setting_section',
			array(
				'type'        => 'text',
				'placeholder' => __( 'United feed name', 'simple-rss-agregator' ),
				'name'        => 'sirsag_title',
			)
		);

		add_settings_field( 'sirsag_desc',
			__( 'Description of the merged feed', 'simple-rss-agregator' ),
			__NAMESPACE__ . '\setting_callback_function',
			'sirsag_feed',
			'sirsag_setting_section',
			array(
				'type'        => 'textarea',
				'placeholder' => __( 'My description', 'simple-rss-agregator' ),
				'name'        => 'sirsag_desc',
			)
		);

		add_settings_field( 'sirsag_lang',
			__( 'Feed language', 'simple-rss-agregator' ),
			__NAMESPACE__ . '\setting_callback_function',
			'sirsag_feed',
			'sirsag_setting_section',
			array(
				'type'        => 'text',
				'placeholder' => 'en-US',
				'name'        => 'sirsag_lang',
			)
		);

		add_settings_field( 'sirsag_items_count',
			__( 'Number of RSS items to display', 'simple-rss-agregator' ),
			__NAMESPACE__ . '\setting_callback_function',
			'sirsag_feed',
			'sirsag_setting_section',
			array(
				'type'        => 'number',
				'placeholder' => '25',
				'name'        => 'sirsag_items_count',
				'default'     => '25',
			)
		);

		echo '<form method="post" action="options.php">';
			settings_fields( 'sirsag_feed' );
			do_settings_sections( 'sirsag_feed' );

			echo '<p class="submit">';
				submit_button( '', 'primary large', 'submit', false );
				echo ' <a href="' . esc_attr( admin_url( 'admin-post.php?action=sirsag.update_cache' ) ) . '" class="button secondary large">' . esc_html( __( 'Update feed cache', 'simple-rss-agregator' ) ) . '</a>';
			echo '</p>';
		echo '</form>';

	echo '</div>';
}


/**
 * Setting callback function for settings
 *
 * @param  [array] $args settings field args
 */
function setting_callback_function( $args ) {

	extract( $args );
	$value_old = get_option( $name, '' );
	switch ( $type ) {
		case 'url' :
		case 'number' :
		case 'text' :
			printf( '<input class="widefat" type="%4$s" name="%1$s" id="%1$s" value="%2$s"%3$s/>',
			    esc_attr( $name ),
			    ( $value_old ? esc_attr( $value_old ) : esc_attr( $default ) ),
				( isset( $placeholder ) ? ' placeholder="' . esc_attr( $placeholder ) . '"' : '' ),
				esc_attr( $type )
			);
			break;
		case 'textarea' :
			printf( '<textarea class="widefat" name="%1$s" id="%1$s"%3$s>%2$s</textarea>',
			    esc_attr( $name ),
			    ( $value_old ? esc_attr( $value_old ) : esc_attr( $default ) ),
			    ( isset( $placeholder ) ? ' placeholder="' . esc_attr( $placeholder ) . '"' : '' )
			);
			break;
		default :
			printf( '<select name="%s" id="%s">',
			    esc_attr( $name )
			);
			foreach ( $options as $key => $option ) {
				printf( '<option value="%1$s"%2$s>%3$s</option>',
					esc_attr( $key ),
					selected( $value_old == $key, true, false ),
					esc_html( $option )
				);
			}
			echo '</select>';
	}
}


/**
 * Reorder menu
 */
add_filter( 'custom_menu_order', __NAMESPACE__ . '\menu_order', PHP_INT_MAX );
function menu_order( $order ) {
	global $submenu;
	$arr = array(
			$submenu['edit.php?post_type=feed'][11],
			$submenu['edit.php?post_type=feed'][5],
			$submenu['edit.php?post_type=feed'][10],
		);
	$submenu['edit.php?post_type=feed'] = $arr;
	return $order;
}


/**
 * Query notification on save options
 */
add_action( 'updated_option', __NAMESPACE__ . '\on_update_option' );
function on_update_option( $option ) {
	if ( in_array( $option, array(
			'sirsag_url',
			'sirsag_lang',
			'sirsag_items_count',
			)
	) ) {
		update_option( 'sirsag_options_updated', 'yes' );
	}
}


/**
 * Notify on save options
 */
add_action( 'admin_notices', __NAMESPACE__ . '\admin_notices' );
function admin_notices() {
	if ( 'yes' == get_option( 'sirsag_options_updated' ) ) {
		echo '<div class="notice notice-success is-dismissible"><p>'
		. esc_html( __( 'Simple feed agregator options saved', 'simple-rss-agregator' ) )
		. '</p></div>';
		flush_rewrite_rules();
		delete_option( 'sirsag_options_updated' );
	}

	if ( 'yes' == get_option( 'sirsag_cache_updated' ) ) {
		echo '<div class="notice notice-success is-dismissible"><p>'
		. esc_html( __( 'Simple feed agregator cache updated', 'simple-rss-agregator' ) )
		. '</p></div>';
		delete_option( 'sirsag_cache_updated' );
	}
}


/**
 * Retrieve Feed name depending on the URI feed
 *
 * @param  [string] $url Feed URI.
 * @return [string]      Feed name.
 */
function get_sitename_by_url( $url ) {
	$url = untrailingslashit( $url );
	$site = get_posts( array(
		'post_type'        => 'feed',
		'posts_per_page'   => 1,
		'suppress_filters' => false,
		'no_found_rows'    => 1,
		'meta_query'       => array( array( 'key' => '_website_uri', 'value' => $url ) ),
	) );
	if ( $site ) {
		return $site[0]->post_title;
	}
	return false;
}
